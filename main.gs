/*
* Gmail検索キーワードによるリスト取得→Chat通知
* Author  :Kawagoe
* Version :0.1b
* Create  :0.1   2016/06/02 新規作成
* Update  :0.1a  2016/09/11 本文処理除去
*         :0.1b  2017/11/27 kawagoe configシートにてシート名別対応
*         :0.2b  2017/12/04 kawagoe 列ソート機能追加
* Etc     :
*/
var App = {
  title               : "自動エントリーシステム",
  debug               : 1, //0:default メール処理有効、0以外:メール無効、既読無効
  startRow            : 3,
  startCol            : 6,
  delimiter           : "|",
  endStr              : "MailEnd",
  checkID             : "TARGETDATA",
  separate            : "------------------------------------------------\n"
}

var Cfg = {
  logName            : "log",
  colRange           : "A2:A",
  colPosition        : [28,28,28,28,28,28,86,145,222,40,120,115,28,140,140],
  startRow           : 2,
  startCol           : 1,
  configName         : "config",
  sheetID            : "1F58Jf8CTyH4sdE9oeC41SCE9qO-POqlSzf_ORS7elBs",
  sheetName          : "",　//configシートから取り込み //update1.1
  execMin            : 5,
  baseCol            : 7,      //No., time, subject, maillink, 
  maxThds            : 500, //googlemax:500
  toastTitle         : "Information",
  systemMailAddress  : "kawagoe@street-smart.co.jp" //処理後通知アドレス(複数は「,」区切り対応）
}

/*
#####################################################
# メニュー
#####################################################
*/
function onOpen(e) {
  var ui = SpreadsheetApp.getUi();
  ui.createMenu('[Custom Menu]')
  .addItem('メールをチェックし、シートへ出力', 'readGmailToOutputSheets')
  .addItem('全シートの列をソート', 'sizeSort')
  .addToUi();
}

/*
#####################################################
# 列サイズ
#####################################################
*/
function sizeSort() {
//  var ss = SpreadsheetApp.getActive();
  var ss = SpreadsheetApp.openById(Cfg.sheetID);
  var sheetList = ss.getSheetByName(Cfg.configName).getRange(Cfg.colRange).getValues();
  
  for ( var i in sheetList ){
    if ( sheetList[i] == "" ) { continue; };
    Logger.log("i:[" + i + "] " + sheetList[i]);
    for ( var j in Cfg.colPosition ){
      Logger.log("j:[" + j + "] " + Cfg.colPosition[j]);
      var cnt = Number(j) + 1;
      ss.getSheetByName(sheetList[i]).setColumnWidth(cnt, Cfg.colPosition[j]);
    }
  }
}

/*
#####################################################
# 検索ワード取得
#####################################################
*/
function inputBox(){
  var value = "";
  value = Browser.inputBox("Enter your search word \\n ex.)has:nouserlabels is:unread, \\n in:anywhere");
  Logger.log(value);
  return value
}

function sendSystemEmail(address, subject, data){
  GmailApp.sendEmail(address, subject, data);
  Logger.log("INFO: " + "Email send.");
}

/*
#####################################################
# config取得
#####################################################
*/
function readConfig() {
  try{
    var startTime = new Date();
//    var ss = SpreadsheetApp.getActive();
    var ss = SpreadsheetApp.openById(Cfg.sheetID);
    var ssUrl = ss.getUrl();
    var cfgSheet = ss.getSheetByName(Cfg.configName);
    var startRow = Cfg.startRow;
    var startCol = Cfg.startCol;
    var lastRow = cfgSheet.getLastRow() - 1;
    var getCfgDatas = cfgSheet.getRange(startRow, startCol, lastRow, 3).getValues();
    for ( var index = getCfgDatas.length - 1; 0 <= index; index-- ) { //sheet check
      var ret = ss.getSheetByName(getCfgDatas[index][0]);
      if ( ret == null ) {
        var errorLog = getCfgDatas[index][0] + " sheet is null";
        LogSheet("WARNING", errorLog);
        Logger.log(errorLog);
        getCfgDatas.pop();
      }
    }

  }catch(e){
    getCfgDatas = false;
    var errorLog = e.name + " : " + e.lineNumber + " : " + e.message;
    LogSheet("ERROR", errorLog);
    sendSystemEmail(Cfg.systemMailAddress, "【" + App.title + "】: エラー", "エラー: " + startTime + "\n\n" + errorLog + "\n\n---\n集計シートURL：" + ssUrl);
  }
  Logger.log(getCfgDatas);
  return getCfgDatas; //[sheetname, searchWord]
}

/*
#####################################################
# Gmail読み込み
#####################################################
*/
function readGmailToOutputSheets() {
  var objLabel = GmailApp.getUserLabelByName("GAS");
  if ( objLabel == null ) {
    GmailApp.createLabel("GAS");
    objLabel = GmailApp.getUserLabelByName("GAS");
  }
  
  LogSheetClear();
  //現在時刻の取得
  var startTime = new Date();
//    var ss = SpreadsheetApp.getActive();
  var ss = SpreadsheetApp.openById(Cfg.sheetID);
  var ssUrl = ss.getUrl();
  
  var cfgDatas = readConfig();
  if ( cfgDatas == false ) {
    var errorLog = "設定シートエラー。シートの取得に失敗しました。設定シートを確認して下さい。";
    LogSheet("ERROR", errorLog);
    sendSystemEmail(Cfg.systemMailAddress, "【" + App.title + "】: エラー", "エラー" + startTime + "\n\n" + errorLog + "\n\n---\n集計シートURL：" + ssUrl);
    return;
  }
  
  var infoLog = "";
  try{
    for ( var index = 0 ; index <= cfgDatas.length - 1; index++ ){
      if (cfgDatas[index][2] !== "ON") {
        Logger.log("対象外: ■" + cfgDatas[index][0] + "シート");
        LogSheet("INFO", "対象外: ■" + cfgDatas[index][0] + "シート");
        continue;
      }
      infoLog = infoLog + "■" + cfgDatas[index][0] + "\n";
      Logger.log("処理中: ■" + cfgDatas[index][0] + "シート処理中");
      LogSheet("INFO", "処理中: ■" + cfgDatas[index][0] + "シート処理中");
      Cfg.sheetName = cfgDatas[index][0];
      
      var sheet = ss.getSheetByName(Cfg.sheetName);
      //    var inputValue = inputBox();
      var startRow = App.startRow;
      var startCol = App.startCol;
      var column = 5;
      var start = 0; //thread index[0]
      var threadsCnt = 0;
      var maillabel, lastDate, dateTime, message, mailId, subject ,maildata, maillink;
      var maxMaildataCol = 0;
      var arr = [];
      
      
      //https://support.google.com/mail/answer/7190?hl=ja
      //    var searchWord = inputValue;
      var searchWord = cfgDatas[index][1];
      //  var searchWord = "label: from: before: after: ";
      //  var searchWord = "has:nouserlabels is:unread";
      //  var searchWord = "in:anywhere";
      ss.toast("検索データ" + searchWord,"INFO",5);
      LogSheet("INFO", "　検索データ" + searchWord);
      
      var cnt = 0;
      var execData = new Array();
      
      whileA: do {
        //検索結果から最大メール列取得（最大スレッド内のメール数） , recordへのデータ配列化
        var record = new Array();
        var threads = GmailApp.search(searchWord, start, Cfg.maxThds);
        var myMessages = GmailApp.getMessagesForThreads(threads); //スレッドからメールを取得
        threadsCnt = myMessages.length;
        LogSheet("INFO", "　検索結果：" + threadsCnt + "スレッド");
        infoLog = infoLog + "検索結果：" + threadsCnt + "スレッド" + "\n";
        
        if ( threadsCnt == 0 ) {
          LogSheet("INFO", "　該当なし。処理中断。");
          　break whileA;
        }
        //処理時間の計測
        var nowTime = new Date();
        var timeDiff = parseInt((nowTime.getTime() - startTime.getTime()) / (1000));
        LogSheet("INFO", "　Phase1: メッセージ取得 " + timeDiff + " 秒 経過....")
        ss.toast("Phase1: メッセージ取得 " + timeDiff + " 秒 経過....",Cfg.toastTitle,5);
        
        var endRow = sheet.getLastRow() - startRow + 1;
        if ( endRow < 1 ){
          endRow = startRow;
        }else{
          startRow = sheet.getLastRow() + 1;
        }
        var endCol = sheet.getLastColumn() - startCol + 1;
        
        var baseDatas = sheet.getRange(startRow, startCol, endRow, endCol).getValues();
        
        for ( var z = 0 ; z < baseDatas.length ; z++ ){
          if ( baseDatas[z][5] == "" ) {
            var checkRow = z - 1;
            break;
          }
        }
        //指定列の最終行までを全て初期化
        //      if(checkRow != 0){
        //        sheet.getRange(startRow, startCol, sheet.getLastRow() - 1, sheet.getLastColumn()).clearContent();
        //      }
        //      Logger.log(checkRow)
        //      if(checkRow > 1){
        //        startRow = checkRow + 1;
        //      }
        
        //処理時間の計測
        var nowTime = new Date();
        var timeDiff = parseInt((nowTime.getTime() - startTime.getTime()) / (1000));
        LogSheet("INFO", "　Phase2: 初期化 " + timeDiff + " 秒 経過....");
        ss.toast("Phase2: 初期化 " + timeDiff + " 秒 経過....",Cfg.toastTitle,5);
        
        var formats = [new Array()];
        var len = 0;
        for(var i = 0; i < threadsCnt; i++) { //row
          //read thread
          var count = threads[i].getMessageCount();
          for (var j = 0; j < count; j++) {
//            var isUnread = threads[i].getMessages()[j].isUnread();  //update 0.2b comment gmail filterにて実施
//            if (isUnread) {
//            var labels = threads[i].getLabels();
//            Logger.log(labels);
//            if ( labels.indexOf(objLabel) == -1 ) {
              lastDate = threads[i].getLastMessageDate();
              dateTime = lastDate.getFullYear() + "/" + (lastDate.getMonth() + 1) + "/" + lastDate.getDate()
              + " " + lastDate.getHours() + ":" + lastDate.getMinutes() + ":" + lastDate.getSeconds();
              subject = threads[i].getFirstMessageSubject();
              maillink = threads[i].getPermalink();
              maildata = threads[i].getMessages();
              mailId = threads[i].getMessages()[j].getId();
              message = threads[i].getMessages()[j].getPlainBody();
              
              record[cnt] = new Array();
              record[cnt][0] = "=ROW()-" + (App.startRow - 1);        //列データ
              record[cnt][1] = new Date();
              record[cnt][2] = dateTime;
              record[cnt][3] = subject; 
              record[cnt][4] = maillink;
              record[cnt][5] = mailId;
              record[cnt][6] = message;
              var baseCnt = 7; //next Column (header skip)
              
              execData = splitIndex(message, App.delimiter, App.endStr, App.checkID); //データの配列化（フォルダIDは更に配列化）
              
              if ( execData != false ){
                Logger.log("execData.length: " + execData[0].length + " maildata.length: " + maildata.length + " maxMaildataCol: " + maxMaildataCol);
                
                var logData = "";
                for(var k = 0; k < execData[0].length; k++){
                  //                if ( k == 0 ) { record[cnt][baseCnt + k] = "test"; } //処理番号 //update comment shiftで対応
                  record[cnt][baseCnt + k] = execData[0][k];
                  logData = logData + "/" + execData[0][k];
                }
                
                //              Logger.log("■■■RECORD REPORT■■■:" + record); //【DEBUG】
                var endCol = baseCnt + execData[0].length;
                
                if ( formats[0].length == 0 ) { //　初回のみカラム分のフォーマット（書式なし）を作成
                  for ( var m = 0; m < endCol ; m++ ) {
                    formats[0].push("@");
                  } //for k
                }
                
                //書込み
                sheet.getRange(startRow + cnt, startCol, 1, endCol).setValues([record[cnt]]).setNumberFormats(formats);
                //ラベル付与 処理後既読にする★
                threads[i].moveToArchive().markRead().addLabel(objLabel);
                
                infoLog = infoLog + "●処理済み:" + subject + " " + logData + "\n";
                
                execData[0].splice(0,1);
                infoLog = infoLog +  "取得データ[ " + i + " ] " + execData + " " + record[cnt][1] + " " + record[cnt][2] + " " + record[cnt][4] + "\n";
                cnt++;
              } //setValues
              else{
                Logger.log("対象データなし: [" + App.checkID + "] : " + i + " " + App.checkID + "が含まれていません");
                infoLog = infoLog + App.separate + "×データなし:" + subject + " " + App.checkID + "が含まれていません" + "\n";
              } //if execData
//            } //if unread
          } //for j
          
          //count thread（未使用
          //        if( maxMaildataCol <= myMessages[i].length){
          //          Logger.log("updateMax: [ " + i + " ]: " + myMessages[i].length);
          //          maxMaildataCol = myMessages[i].length;
          //        }
          
          //処理時間の計測
          var nowTime = new Date();
          var timeDiff = parseInt((nowTime.getTime() - startTime.getTime()) / (1000));
          LogSheet("INFO", "　Phase3: debug 取得データ[ " + i + " ] " + timeDiff + " 秒 経過....")
          //          ss.toast("Phase3: debug 取得データ[ " + i + " ] " + timeDiff + " 秒 経過....",Cfg.toastTitle,5);
          
        } //for i
        
        start += threadsCnt;
      } while(threadsCnt >= Cfg.maxThds); //MAX500のスレッド検索結果が有る場合、続きから検索:start
      
      //処理時間の計測
      var nowTime = new Date();
      var timeDiff = parseInt((nowTime.getTime() - startTime.getTime()) / (1000));
      LogSheet("INFO", "　Phase4: 処理完了　" + timeDiff + " 秒 経過....")
      ss.toast((startRow + i) + " / " + endCol + " " + "Phase4: 処理完了　" + timeDiff + " 秒 経過....",Cfg.toastTitle,5);
      
    } //for index
    
  }catch(e){
    LogSheet("ERROR", e.name + " : " + e.lineNumber + " : " + e.message)
    throw new Error("エラー: " + e.name + " : " + e.lineNumber + " : " + e.message);
  }
  
  //結果を通知
  LogSheet("INFO", "処理終了")
  sendSystemEmail(Cfg.systemMailAddress, "【" + App.title + "】: 処理完了", "処理完了" + startTime + "\n\n" + infoLog + "\n\n---\n集計シートURL：" + ssUrl);
}
